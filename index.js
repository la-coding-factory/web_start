/* ************************************************************************** */
/*                                                                            */
/*                                                       ██████╗███████╗      */
/*   index.js                                           ██╔════╝██╔════╝      */
/*                                                      ██║     █████╗        */
/*   By: erucquoy <edouard.rucquoy@edu.itescia.f        ██║     ██╔══╝        */
/*                                                      ╚██████╗██║           */
/*   Created: 2021/12/01 10:12:55 by erucquoy            ╚═════╝╚═╝           */
/*   Updated: 2021/12/03 07:34:31 by erucquoy         codingfactory.fr        */
/*                                                                            */
/* ************************************************************************** */

import express from 'express'
import http from 'http'
import nunjucks from 'nunjucks'
import passport from 'passport'
import cors from 'cors'
import expressSession from 'express-session'
import config from './config.js'
import apiRouter from './router.js'
import { startMongoose } from './db/mongo.js'
import { Server as SioServer } from 'socket.io'
import { createSocketHandler } from './socket-io.js'
import './auth/passport.js'
import './workers/startWorkers.js'
import { middlewareTokenAuth } from './auth/tokens.js'

startMongoose()
    .then(() => {
        console.log('Connected to DB');
        startWebServer()
    })
    .catch((err) => {
        console.error(err)
    })

function startWebServer() {

    const app = express()
    const server = http.createServer(app)
    const io = new SioServer(server)

    app.use(cors({
        origin: [
            'http://127.0.0.1:3010',
            'http://localhost:3010'
        ],
        credentials: true
    }))

    io.on('connection', createSocketHandler(io))

    nunjucks.configure('views', {
        autoescape: true,
        express: app,
    })

    /* Token middleware */
    app.use(middlewareTokenAuth)

    /* Express app config */
    //app.use(cookieParser())
    app.use(expressSession({ secret: 'super secret!', resave: false, saveUninitialized: false, httpOnly: false }))
    app.use(passport.initialize())
    app.use(passport.authenticate('session'))
    app.use((req, res, next) => {
        console.log(req.url)
        next();
    })

    // Servir des fichiers statiques
    app.use(express.static('public'))
    // parse le body de la requete en objet disponible ensuite dans req.body sous forme d'objet
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))

    app.use(apiRouter)

    server.listen(config.PORT, config.HOST, () => {
        console.log(`Listening on http://${config.HOST}:${config.PORT}`)
    })

}
