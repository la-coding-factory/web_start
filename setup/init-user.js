//
import mongoose from 'mongoose'
import config from '../config.js'
import { startMongoose } from '../db/mongo.js'

import { User } from '../db/models'


const DEFAULT_USER = {
	email: 'Default@gg.com',
	password: 'test'
}

async function init() {
	const verif = await User.verify(DEFAULT_USER.email, DEFAULT_USER.password)
	console.log(verif)

	const user = await User.findOne({ email: DEFAULT_USER.email.toLowerCase() })
	if (user) {
		console.log('User already exists', DEFAULT_USER);
		return shutdown()
	}

	const defaultUser = await User.register(DEFAULT_USER.email, DEFAULT_USER.password)
	console.log('Default user created!', defaultUser);

	// exit
	shutdown()
}

function shutdown() {
	mongoose.connection.close()
}

startMongoose().then(init)
