//
import mongoose from 'mongoose'
import config from '../config.js'
import { startMongoose } from '../db/mongo.js'

import { Role } from '../db/models'


const DEFAULT_ROLE = {
	name: 'Default',
	permmissions: []
}

async function init() {
	const roles = await Role.find({})
	if (roles.length !== 0) {
		console.log('Roles already exists', roles);
		return shutdown()
	}

	const defaultRole = await Role.create(DEFAULT_ROLE)
	console.log('Default role created!', defaultRole);

	// exit
	shutdown()
}

function shutdown() {
	mongoose.connection.close()
}

startMongoose().then(init)
