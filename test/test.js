import axios from 'axios'
import config from '../config.js'
import assert from 'assert'

/* TEST SERVICES */

/* TEST API */

axios.defaults.baseURL = `http://${config.HOST}:${config.PORT}`
axios.defaults.validateStatus = function validateStatus() {
    return true;
}

describe('Todos API', function() {
    describe('POST /todos', function() {
        it('Good: should create a new todo and return it', async function() {
            const result = await axios.post('/api/todos', {
                name: 'Ma premiere Todo',
                items: []
            })
            assert.equal(200, result.status)
            assert.deepEqual(['name', '_id', 'createdAt', 'updatedAt', 'items', '__v'].sort(), Object.keys(result.data).sort())
        })

        it(`Without 'name': should not create a new todo and return errors`, async function() {
            const result = await axios.post('/api/todos', {
                name: '',
                items: []
            })
            assert.equal(400, result.status)
            assert.deepEqual(['error'], Object.keys(result.data))
        })
    })
})

