import config from "../config.js";
import { Room, Message } from '../db/models/index.js';
import { startMongoose } from '../db/mongo.js'

async function test() {
	const room = await Room.find({ name: 'Room A' })
	const firstMessage = await Message.create({ text: 'Bonjour!', room: room._id })


	console.log({
		room, firstMessage
	})

	const room_recup = await Room.findOne({ _id: room._id }).populate('messages')

	console.log({ room_recup })

}

startMongoose().then(test)
