import express from 'express'
import passport from 'passport'
import { chatController, formController, helloController, isLoggedInController, loginAjaxController, logoutController, welcomeController, loginTokenController } from './controllers.js'
import { deleteTodosController, getAllTodosController, getOneTodosController, patchTodosController, postTodosController, putTodosController } from './api/todos/todos.controller.js';
import mustBeAuth from './auth/mustBeAuth.js';


const mw_test = (req, res, next) => {
    console.log(req.url)
    next();
}

const router = express.Router()

/* Views */
router.get('/', helloController)
router.get('/welcome', mustBeAuth, welcomeController)
router.get('/chat', mustBeAuth, chatController)
router.post('/form', mw_test, formController)
router.get('/me', mustBeAuth, isLoggedInController)

/* Auth system */
router.post('/login', passport.authenticate('local', {
    failureRedirect: '/login',
    successRedirect: '/welcome'
}))
router.post('/login-ajax', passport.authenticate('local'), loginAjaxController)
router.post('/login-token', loginTokenController)
router.get('/logout', logoutController)

/* API TODOS */
router.get('/todos', getAllTodosController)
router.get('/todos/:id', getOneTodosController)
router.post('/todos', postTodosController)
router.patch('/todos', patchTodosController) // modifie partiellement
router.delete('/todos', deleteTodosController)
router.put('/todos', putTodosController) // remplace la ressource

export default router
