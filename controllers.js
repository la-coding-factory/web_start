import { newClientToken } from "./auth/tokens.js"

export function helloController(req, res) {
    console.log(req.query) // /hello?query=string { query: "string" }
    res.render('home.njk', {
        title: 'Home Page',
        user: (req.user)
    })
}

export function welcomeController(req, res) {
    console.log(req.query) // /hello?query=string { query: "string" }
    res.render('home.njk', {
        title: 'Home Page',
        user: (req.user)
    })
}

export function loginAjaxController(req, res) {
    res.json({
        user: req.user,
    })
}

export function logoutController(req, res) {
    req.logout();
    res.redirect('/')
}

export function formController(req, res) {
    console.log(req.body)
    res.json({
        success: true,
    })
}

export async function chatController(req, res) {
    const clientToken = await newClientToken(req.user._id)

    res.render('chat.html', {
        clientToken: clientToken,
        title: 'Chat page',
        user: req.user,
    })
}

export function isLoggedInController(req, res) {
    res.json({
        user: req.user,
    })
}

export async function loginTokenController(req, res) {
    const { clientToken } = await verifyAndCreateToken(req.body)
    res.json({
        clientToken
    })
}
