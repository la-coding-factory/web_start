import url from 'url'
import { getUserByToken } from './auth/tokens.js'

export function createSocketHandler(io) {

	return function socketHandler(socket) {
		console.log('io client connected', socket.id)

		// const cookies = new url.URLSearchParams(socket.request.headers.cookie);
		// console.log({ getCookieTest: cookies.get('connect.sid') })

		console.log(socket.id)

		socket.data_room = 'general'
		socket.join(socket.data_room)
		socket.data_pseudo = 'Default pseudo'

		/* Quand le client envoi "event-name" le callback associé est déclenché */
		socket.on('event-name', (data) => {
			console.log(socket.id, 'sent "event-name" with data:', data)
			// io.emit('event-name', { server: 'coucou' })
			socket.emit('levent-que-lon-souhaite')
		})

		socket.on('client:token', async (data) => {
			if (!data.token) { return; }
			const user = await getUserByToken(data.token)
			socket.data_clientToken = data.token;
			socket.user = user;
			socket.data_pseudo = user.email;
		})

		socket.on('message:new', (data) => {
			if (!socket.data_pseudo) { return ; }
			data.pseudo = socket.data_pseudo
			io.to(socket.data_room).emit('message:new', { text: data.text, username: socket.data_pseudo })
		})

		socket.on('client:pseudo', (data) => {
			if (!data.pseudo || data.pseudo.length < 3) { return ; }
			socket.data_pseudo = data.pseudo
		})

		socket.on('room:change', data =>  {
			if (!data.name) { return ; }
			socket.leave(socket.data_room)
			socket.data_room = data.name
			socket.join(socket.data_room)
		})
	}
}
