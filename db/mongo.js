import moongose from 'mongoose'
import config from '../config.js'

export function startMongoose() {
    return moongose.connect(config.MONGO_URI)
}