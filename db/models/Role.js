import mongoose from 'mongoose'

const schema = new mongoose.Schema({
    name: { type: String },
	level: { type: Number },

	permissions: {
		type: [String]
	}
}, {
    timestamps: true,
    minimize: false,
})

const Role = mongoose.model('Role', schema)

export default Role
