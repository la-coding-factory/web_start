import mongoose from 'mongoose'
import crypto from 'crypto'
import UserClientToken from './UserClientToken.js'

const schema = new mongoose.Schema({
    email: { type: String },
    password: { type: String },
    role: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Role'
	}
}, {
    timestamps: true,
    minimize: false,
})

/**
 * Clean data and create account
 * @param {String} email
 * @param {String} password
 */
function register (email, password) {
	email = email.toLowerCase()
	password = crypto.createHash('sha512').update(password).digest('hex')
	return this.create({ email, password })
}

/**
 * Clean data and verify if account exist with good password
 * @param {String} email
 * @param {String} password
 */
function verify (email, password) {
	console.log(email, password)
	email = email.toLowerCase()
	password = crypto.createHash('sha512').update(password).digest('hex')
	return this.findOne({ email, password }, 'email createdAt updatedAt')
}

/**
 * User's password verification
 * @param {String} password
 * @returns
 */
function verifyPassword (password) {
	password = crypto.createHash('sha512').update(password).digest('hex')
	return (this.password === password)
}

schema.static('register', register)
schema.static('verify', verify)
schema.method('verifyPassword', verifyPassword)

const User = mongoose.model('User', schema)

export default User
