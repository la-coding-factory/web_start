import mongoose from 'mongoose'
import crypto from 'crypto'

const schema = new mongoose.Schema({
    clientToken: { type: String, default: () => crypto.randomBytes(24).toString('hex') },
    user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	}
}, {
    timestamps: true,
    minimize: false,
})

const UserClientToken = mongoose.model('UserClientToken', schema)

export default UserClientToken
