import mongoose from 'mongoose'

const schema = new mongoose.Schema({
	room: { type: mongoose.Schema.Types.ObjectId, ref: 'Room' },
	text: { type: String }
}, {
	timestamps: true,
	minimize: false,
})

const Message = mongoose.model('Message', schema)

export default Message
