import mongoose from 'mongoose'

const schema = new mongoose.Schema({
	name: { type: String },

	messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message' }]
}, {
	timestamps: true,
	minimize: false,
})

const Room = mongoose.model('Room', schema)

export default Room
