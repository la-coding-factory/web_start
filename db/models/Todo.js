import mongoose from 'mongoose'

const schema = new mongoose.Schema({
    name: { type: String, default: "" },
    items: { type: [String] }
    /* option timestamps, ajoutera "createdAt" et "updatedAt" */
}, {
    timestamps: true,
    minimize: false,
})

const Todo = mongoose.model('Todo', schema)

export default Todo