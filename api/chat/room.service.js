import { Room, Message } from '../../db/models/index.js'

export async function create(name = 'Default') {
	let room;
	try {
		room = await Room.create({
			name
		})
	} catch (err) {
		console.error(err)
		return false;
	}
	return room;
}

