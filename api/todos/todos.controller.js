/**
 * Les controllers doivent utiliser les Services pour acceder a la BDD
 */

import { createTodo } from "./todos.service.js"
import { checkPostTodos } from "./todos.validator.js"

export function getAllTodosController(req, res) {

}

export function getOneTodosController(req, res) {
    console.log(req.params) // object { id: "xxx" }
    res.json({
        id: req.params.id
    })
}

export async function postTodosController(req, res) {
    const check = checkPostTodos(req.body)
    if (check !== true) {
        return res.status(400).json({
            error: check
        })
    }
    const todo = await createTodo(req.body.name, req.body.items)
    res.json(todo)
}

export function patchTodosController(req, res) {
    
}

export function putTodosController(req, res) {
    
}

export function deleteTodosController(req, res) {
    
}