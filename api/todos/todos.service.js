import Todo from './db/models/Todo.js'

/**
 * Create and save a new Todo List
 * @param {String} name 
 * @param {Array} items 
 * @returns 
 */
export async function createTodo(name, items = []) {
    const todo = await Todo.create({
        name: name,
        items: items,
    })
    return todo;
}

createTodo()