import User from '../db/models/User.js'
import UserClientToken from "../db/models/UserClientToken.js";

/**
 * Object containing 'email' and 'password'. Body-data-like.
 * @typedef {Object} EmailPasswordObject
 * @property {String} email
 * @property {String} password
 */

/**
 * Object containing 'clientToken' and 'user'
 * @typedef {Object} TokenAndUser
 * @property {Object} user
 * @property {String} clientToken
 */

/**
 *
 * @param {String} userId
 * @returns {String} clientToken
 */
export async function newClientToken(userId) {
	const userClientToken = await UserClientToken.create({ user: userId })
	return userClientToken.clientToken;
}

/**
 * Verify email and password, and returns { clientToken, user }
 * @param {EmailPasswordObject} payload
 * @returns {TokenAndUser}
 */
export async function verifyAndCreateToken({ email, password }) {
	const user = await User.verify(email, password)
	if (!user) {
		return { error: 'Bad login' };
	}
	const clientToken = await newClientToken(user._id)
	return { clientToken, user }
}

/**
 * Return an User document by providing a clientToken string
 * @param {String} clientToken
 */
export async function getUserByToken(clientToken) {
	const userToken = await UserClientToken.findOne({ clientToken }).populate({ path: 'user', select: 'email _id createdAt updatedAt' })
	return userToken.user
}

/**
 * Express middleware to manage authentication by clientToken
 * @param {*} req - Express request
 * @param {*} res - Express response
 * @param {*} next - Express middleware 'next' callback
 */
export async function middlewareTokenAuth(req, res, next) {
	if (req.query.clientToken) {
		const user = await getUserByToken(req.query.clientToken)
		if (user) req.user = user;
	}
	else if (req.headers['x-client-token']) {
		const user = await getUserByToken(req.headers['x-client-token'])
		if (user) req.user = user;
	}
	next();
}
