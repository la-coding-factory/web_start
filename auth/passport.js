import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { User } from '../db/models/index.js'

passport.use(new LocalStrategy(
	{
		usernameField: 'email',
	},
	async function (email, password, done) {
		const user = await User.verify(email, password)
		if (!user) {
			return done(false, false, { message: 'Incorrect username or password' })
		}
		return done(false, user)
	}
))

// passport.use(new LocalStrategy(
// 	{
// 		usernameField: 'email',
// 	},
// 	async function (email, password, done) {
// 		const user = await User.verify(email, password)
// 		if (!user) {
// 			return done(false, false, { message: 'Incorrect username or password' })
// 		}
// 		return done(false, user)
// 	}
// ))

passport.serializeUser(function(user, done) {
	done(null, user);
});

passport.deserializeUser(function(user, done) {
	done(null, user);
});
