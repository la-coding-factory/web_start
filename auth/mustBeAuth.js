export default function mustBeAuth(req, res, next) {
	if (req.user && req.user._id) {
		return next();
	}
	res.status(401).end();
}
