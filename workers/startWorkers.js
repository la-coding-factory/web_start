import { isMainThread, Worker } from 'worker_threads'
import config from '../config.js'
import path from 'path'
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
/**
 * References to Worker Threads
 */
const workers = {
	tokens: null,
	mail: null,
}

if (isMainThread) {
	startTokenWorker()
}

async function startTokenWorker() {
	const tokenWorker = new Worker(path.resolve(__dirname, 'expireTokens.js'), {
		workerData: {
			EXPIRATION_TIME_MS: config.TOKEN_EXPIRATION_MS
		}
	})
	workers.tokens = tokenWorker
}
