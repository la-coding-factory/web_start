import { workerData, parentPort } from 'worker_threads'
import cron from 'node-cron'
import { startMongoose } from '../db/mongo.js'
import UserClientToken from '../db/models/UserClientToken.js'

const EXPIRATION_TIME_MS = (workerData.EXPIRATION_TIME_MS) || (60 * 15 * 1000); // 15 minutes

startMongoose().then(() => {
	startWorker()
	// tokenExpirationChecker()
})

async function tokenExpirationChecker() {
	console.log('Check for expired tokens');
	try {
		const expiration = new Date((+new Date()) - EXPIRATION_TIME_MS)
		const expired = await UserClientToken.find({ createdAt: { $lte: expiration }})
		console.log('Tokens expired: ', expired.length)
		const promisesRemove = []
		for(let token of expired) {
			promisesRemove.push(token.remove())
		}
		const removed = await Promise.allSettled(promisesRemove)
		console.log('Removed tokens: ', removed.length)
	}
	catch (err) {
		console.error(`Une erreur est survenue lors de la suppression d'un token utilisateur`)
		console.error(err)
	}
}

function startWorker() {
	console.log('started')
	cron.schedule('* * * * *', tokenExpirationChecker)
}


