Pour le groupe qui utilisait des cookie et 1 front sur un port différent :
```js
axios.defaults.withCredentials = true;
```

```js
express.use(cors({
            origin: [
              'http://localhost:3010',
              'http://localhost:3020'
            ],
            credentials: true,
            exposedHeaders: ['set-cookie']
        }));
```
